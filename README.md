remote-syslog2
=========

This module configures the Papertrail remote_syslog2 agent: https://help.papertrailapp.com/kb/configuration/configuring-centralized-logging-from-text-log-files-in-unix/

Requirements
------------

None.

Role Variables
--------------

TBD.

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: remote-syslog2 }

License
-------

BSD
